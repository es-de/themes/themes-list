# ES-DE Frontend - Themes

Themes are most easily installed using the built-in theme downloader that is accessible from the _UI Settings_ menu. The exception is themes listed under the [Additional themes](https://gitlab.com/es-de/themes/themes-list#additional-themes) section as these need to be manually installed.

All screenshots are in 16:9 aspect ratio at 1920x1080 resolution.

Variants, color schemes, font sizes, aspect ratios, transitions and languages can be changed from the _UI Settings_ menu.

Note that having support for a language may not mean that all text has yet been translated, as complete localization is a long term undertaking.

Table of contents:

[[_TOC_]]

### Adroit
https://github.com/RickAndTired/adroit-es-de

**Created by**\
Rick

**Variants**\
Grid Covers\
Grid Logos\
List\
List No Art

**Color schemes**\
Dark\
Light\
Animated

**Font sizes**\
Medium\
Large

**Aspect ratios**\
16:9\
16:10\
4:3\
19.5:9\
21:9\
1:1

**Transitions**\
Instant\
Slide\
Fade

![alt text](screenshots/adroit-es-de/adroit-es-de_01.jpg "System view, Grid Covers variant, Light color scheme")
_System view, Grid Covers variant, Light color scheme_

![alt text](screenshots/adroit-es-de/adroit-es-de_02.jpg "Gamelist view, Grid Covers variant, Light color scheme")
_Gamelist view, Grid Covers variant, Light color scheme_

![alt text](screenshots/adroit-es-de/adroit-es-de_03.jpg "System view, List variant, Light color scheme")
_System view, List variant, Light color scheme_

![alt text](screenshots/adroit-es-de/adroit-es-de_04.jpg "Gamelist view, List variant, Light color scheme")
_Gamelist view, List variant, Light color scheme_

![alt text](screenshots/adroit-es-de/adroit-es-de_05.jpg "System view, List variant, Dark color scheme")
_System view, List variant, Dark color scheme_

![alt text](screenshots/adroit-es-de/adroit-es-de_06.jpg "Gamelist view, List variant, Dark color scheme")
_Gamelist view, List variant, Dark color scheme_

---

### Alekfull NX (Revisited)
https://github.com/anthonycaccese/alekfull-nx-es-de

**Created by**\
ant and fagnerpc

**Variants**\
List: Metadata & Miximage\
List: Metadata & Boxart\
List: Miximage\
List: Boxart

**Color schemes**\
Light\
Dark\
Light (Custom Images)\
Dark (Custom Images)

**Font sizes**\
Medium\
Large\
Extra large

**Aspect ratios**\
16:9\
16:10\
4:3\
19.5:9

![alt text](screenshots/alekfull-nx-es-de/alekfull-nx-es-de_01.jpg "System view, List: Metadata & Miximage variant, Dark color scheme")
_System view, List: Metadata & Miximage variant, Dark color scheme_

![alt text](screenshots/alekfull-nx-es-de/alekfull-nx-es-de_02.jpg "Gamelist view, List: Metadata & Miximage variant, Dark color scheme")
_Gamelist view, List: Metadata & Miximage variant, Dark color scheme_

![alt text](screenshots/alekfull-nx-es-de/alekfull-nx-es-de_03.jpg "System view, List: Metadata & Miximage variant, Light color scheme")
_System view, List: Metadata & Miximage variant, Light color scheme_

![alt text](screenshots/alekfull-nx-es-de/alekfull-nx-es-de_04.jpg "Gamelist view, List: Metadata & Miximage variant, Light color scheme")
_Gamelist view, List: Metadata & Miximage variant, Light color scheme_

---

### Analogue OS Menu
https://github.com/anthonycaccese/analogue-os-menu-es-de

**Created by**\
ant

**Variants**\
List\
Carousel\
Grid

**Color schemes**\
Dark\
Light\
Custom

**Aspect ratios**\
16:9\
16:10\
3:2\
4:3\
19.5:9\
1:1

![alt text](screenshots/analogue-os-menu-es-de/analogue-os-menu-es-de_01.jpg "System view, Grid variant, Dark color scheme")
_System view, Grid variant, Dark color scheme_

![alt text](screenshots/analogue-os-menu-es-de/analogue-os-menu-es-de_02.jpg "Gamelist view, Grid variant, Dark color scheme")
_Gamelist view, Grid variant, Dark color scheme_

![alt text](screenshots/analogue-os-menu-es-de/analogue-os-menu-es-de_03.jpg "System view, Carousel variant, Light color scheme")
_System view, Carousel variant, Light color scheme_

![alt text](screenshots/analogue-os-menu-es-de/analogue-os-menu-es-de_04.jpg "Gamelist view, Carousel variant, Light color scheme")
_Gamelist view, Carousel variant, Light color scheme_

---

### Art Book Next
https://github.com/anthonycaccese/art-book-next-es-de

**Created by**\
ant

**Variants**\
List: Metadata & Miximage\
List: Metadata & Boxart\
List: Metadata, Screenshot & Marquee\
List: Boxart\
List: Screenshot & Marquee\
[Help Off] List: Metadata & Miximage\
[Help Off] List: Metadata & Boxart\
[Help Off] List: Metadata, Screenshot & Marquee\
[Help Off] List: Boxart\
[Help Off] List: Screenshot & Marquee

**Color schemes**\
Dark [Screenshots]\
Dark [Noir]\
Dark [Circuit]\
Dark [Original Screenshots]\
Light [Screenshots]\
Light [Noir]\
Light [Circuit]\
Light [Original Screenshots]\
Steam OS [Screenshots]\
Steam OS [Noir]\
Steam OS [Circuit]\
Steam OS [Original Screenshots]\
SNES [Screenshots]\
SNES [Noir]\
SNES [Circuit]\
SNES [Original Screenshots]\
Famicom [Screenshots]\
Famicom [Noir]\
Famicom [Circuit]\
Famicom [Original Screenshots]\
OLED [Screenshots]\
OLED [Noir]\
OLED [Circuit]\
OLED [Original Screenshots]\
Custom

**Font sizes**\
Medium\
Large\
Small

**Aspect ratios**\
16:9\
16:10\
3:2\
4:3\
5:4\
19.5:9\
21:9\
32:0\
1:1

![alt text](screenshots/art-book-next-es-de/art-book-next-es-de_01.jpg "System view, List: Metadata & Miximage variant, Dark [Screenshots] color scheme")
_System view, List: Metadata & Miximage variant, Dark [Screenshots] color scheme_

![alt text](screenshots/art-book-next-es-de/art-book-next-es-de_02.jpg "Gamelist view, List: Metadata & Miximage variant, Dark [Screenshots] color scheme")
_Gamelist view, List: Metadata & Miximage variant, Dark [Screenshots] color scheme_

![alt text](screenshots/art-book-next-es-de/art-book-next-es-de_03.jpg "Gamelist view, List: Screenshot & Marquee variant, Steam OS [Screenshots] color scheme")
_Gamelist view, List: Screenshot & Marquee variant, Steam OS [Screenshots] color scheme_

---

### Artflix (Revisited)
https://github.com/TheGrizzMD/artflix-revisited-es-de

**Created by**\
TheGrizzMD and Alekfull

**Variants**\
Default\
Default Immersive\
Covers Carousel\
Covers Carousel Immersive

**Font sizes**\
Medium\
Large

**Aspect ratios**\
16:9\
16:10\
4:3\
19.5:9

![alt text](screenshots/artflix-revisited-es-de/artflix-revisited-es-de_01.jpg "System view, Default variant")
_System view, Default variant_

![alt text](screenshots/artflix-revisited-es-de/artflix-revisited-es-de_02.jpg "Gamelist view, Default variant")
_Gamelist view, Default variant_

![alt text](screenshots/artflix-revisited-es-de/artflix-revisited-es-de_03.jpg "Gamelist view, Default Immersive variant")
_Gamelist view, Default Immersive variant_

![alt text](screenshots/artflix-revisited-es-de/artflix-revisited-es-de_04.jpg "System view, Covers Carousel variant")
_System view, Covers Carousel variant_

![alt text](screenshots/artflix-revisited-es-de/artflix-revisited-es-de_05.jpg "Gamelist view, Covers Carousel Immersive variant")
_Gamelist view, Covers Carousel Immersive variant_

---

### Atari 50 Menu
https://github.com/anthonycaccese/atari-50-menu-es-de

**Created by**\
ant

**Aspect ratios**\
16:9\
16:10\
4:3

![alt text](screenshots/atari-50-menu-es-de/atari-50-menu-es-de_01.jpg "System view")
_System view_

![alt text](screenshots/atari-50-menu-es-de/atari-50-menu-es-de_02.jpg "Gamelist view")
_Gamelist view_

---

### Aura
https://github.com/Siddy212/aura-es-de

**Created by**\
Siddy212

**Variants**\
[Carousel] Textlist\
[Carousel] Detailed-Grid\
[Carousel] Detailed-Grid (Help Off)\
[Carousel] Basic-Grid\
[Carousel] Basic-Grid (Help Off)\
[Carousel] Carousel-Marquee\
[Carousel] Carousel-Boxart\
[Fullscreen] Textlist\
[Fullscreen] Detailed-Grid\
[Fullscreen] Detailed-Grid (Help Off)\
[Fullscreen] Basic-Grid\
[Fullscreen] Basic-Grid (Help Off)\
[Fullscreen] Carousel-Marquee\
[Fullscreen] Carousel-Boxart\
[Grid] Textlist\
[Grid] Detailed-Grid\
[Grid] Detailed-Grid (Help Off)\
[Grid] Basic-Grid\
[Grid] Basic-Grid (Help Off)\
[Grid] Carousel-Marquee\
[Grid] Carousel-Boxart

**Color schemes**\
[Art] Light Glass\
[Art] Dark Glass\
[Art] Purple\
[Art] Blue\
[Art] Green\
[Art] Orange\
[Art] Black\
[Logo] Purple\
[Logo] Blue\
[Logo] Green\
[Logo] Orange\
[Logo] Black

**Font sizes**\
Medium\
Large\
Small\
Extra large

**Aspect ratios**\
16:9\
16:10\
3:2\
4:3\
19.5:9\
21:9\
1:1

![alt text](screenshots/aura-es-de/aura-es-de_01.jpg "System view, [Carousel] Textlist variant, [Art] Light Glass color scheme")
_System view, [Carousel] Textlist variant, [Art] Light Glass color scheme_

![alt text](screenshots/aura-es-de/aura-es-de_02.jpg "Gamelist view, [Carousel] Textlist variant, [Art] Light Glass color scheme")
_Gamelist view, [Carousel] Textlist variant, [Art] Light Glass color scheme_

![alt text](screenshots/aura-es-de/aura-es-de_03.jpg "System view, [Grid] Detailed-Grid variant, [Logo] Blue color scheme")
_System view, [Grid] Detailed-Grid variant, [Logo] Blue color scheme_

![alt text](screenshots/aura-es-de/aura-es-de_04.jpg "Gamelist view, [Grid] Detailed-Grid variant, [Logo] Blue color scheme")
_Gamelist view, [Grid] Detailed-Grid variant, [Logo] Blue color scheme_

---

### Canvas
https://github.com/Siddy212/canvas-es-de

**Created by**\
Siddy212

**Variants**\
[Grid] Textlist\
[Grid] Textlist - Flipped\
[Grid] Carousel: Marquee\
[Grid] Carousel: Boxart\
[Grid] Grid: Basic\
[Grid] Grid: Detailed\
[Grid] Grid: Detailed - Flipped\
[Grid] Row\
[Carousel] Textlist\
[Carousel] Textlist - Flipped\
[Carousel] Carousel: Marquee\
[Carousel] Carousel: Boxart\
[Carousel] Grid: Basic\
[Carousel] Grid: Detailed\
[Carousel] Grid: Detailed - Flipped\
[Carousel] Row

**Color schemes**\
[Capsule] Dark\
[Capsule] Light\
[Capsule] Retro\
[Capsule] Neon\
[Capsule] Pastel\
[Capsule] Sony\
[Capsule] NES\
[Capsule] SNES\
[Capsule] Famicom\
[Controller] Dark\
[Controller] Light\
[Controller] Retro\
[Controller] Neon\
[Controller] Pastel\
[Controller] Sony\
[Controller] NES\
[Controller] SNES\
[Controller] Famicom\
[Logo] Dark\
[Logo] Light\
[Logo] Retro\
[Logo] Neon\
[Logo] Pastel\
[Logo] Sony\
[Logo] NES\
[Logo] SNES\
[Logo] Famicom\
[Screenshot] Dark\
[Screenshot] Light\
[Screenshot] Retro\
[Screenshot] Neon\
[Screenshot] Pastel\
[Screenshot] Sony\
[Screenshot] NES\
[Screenshot] SNES\
[Screenshot] Famicom\
[Art] Dark\
[Art] Light\
[Art] Retro\
[Art] Neon\
[Art] Pastel\
[Art] Sony\
[Art] NES\
[Art] SNES\
[Art] Famicom

**Font sizes**\
Medium\
Large\
Small\
Extra large

**Aspect ratios**\
16:9\
16:10\
3:2\
4:3\
19.5:9\
21:9\
1:1

![alt text](screenshots/canvas-es-de/canvas-es-de_01.jpg "System view, [Grid] Textlist variant, [Art] Retro color scheme")
_System view, [Grid] Textlist variant, [Art] Retro color scheme_

![alt text](screenshots/canvas-es-de/canvas-es-de_02.jpg "Gamelist view, [Grid] Textlist variant, [Art] Retro color scheme")
_Gamelist view, [Grid] Textlist variant, [Art] Retro color scheme_

![alt text](screenshots/canvas-es-de/canvas-es-de_03.jpg "System view, [Grid] Carousel: Marquee variant, [Art] Dark color scheme")
_System view, [Grid] Carousel: Marquee variant, [Art] Dark color scheme_

![alt text](screenshots/canvas-es-de/canvas-es-de_04.jpg "Gamelist view, [Grid] Carousel: Marquee variant, [Art] Dark color scheme")
_Gamelist view, [Grid] Carousel: Marquee variant, [Art] Dark color scheme_

---

### CarAlt
https://github.com/Weestuarty/caralt-es-de

**Created by**\
Weestuarty

**Variants**\
With videos\
Without videos

**Color schemes**\
Gold\
Black & white\
Blue\
Gameboy\
Gameboy Shell\
Retro\
Stay Classy\
Commodore 64\
Chilled\
Purple Rain\
Rusty\
Sega

**Font sizes**\
Medium\
Large\
Extra large

**Aspect ratios**\
16:9\
16:10\
4:3\
21:9

**Transitions**\
Instant w/Fade\
Instant

![alt text](screenshots/caralt-es-de/caralt-es-de_01.jpg "System view, Chilled color scheme")
_System view, Chilled color scheme_

![alt text](screenshots/caralt-es-de/caralt-es-de_02.jpg "Gamelist view, With videos variant, Chilled color scheme")
_Gamelist view, With videos variant, Chilled color scheme_

![alt text](screenshots/caralt-es-de/caralt-es-de_03.jpg "System view, Retro color scheme")
_System view, Retro color scheme_

![alt text](screenshots/caralt-es-de/caralt-es-de_04.jpg "Gamelist view, Without videos variant, Retro color scheme")
_Gamelist view, Without videos variant, Retro color scheme_

![alt text](screenshots/caralt-es-de/caralt-es-de_05.jpg "System view, Purple Rain color scheme")
_System view, Purple Rain color scheme_

![alt text](screenshots/caralt-es-de/caralt-es-de_06.jpg "Gamelist view, With videos variant, Purple Rain color scheme")
_Gamelist view, With videos variant, Purple Rain color scheme_

---

### Carbon
https://github.com/lilbud/carbon-es-de

**Created by**\
lilbud and Rookervik

**Color schemes**\
Red\
Blue\
Green\
Yellow\
Orange\
Purple\
Teal\
Pink

**Font sizes**\
Medium\
Large

![alt text](screenshots/carbon-es-de/carbon-es-de_01.jpg "System view, Red color scheme")
_System view, Red color scheme_

![alt text](screenshots/carbon-es-de/carbon-es-de_02.jpg "Gamelist view, Red color scheme")
_Gamelist view, Red color scheme_

![alt text](screenshots/carbon-es-de/carbon-es-de_03.jpg "System view, Teal color scheme")
_System view, Teal color scheme_

![alt text](screenshots/carbon-es-de/carbon-es-de_04.jpg "Gamelist view, Teal color scheme")
_Gamelist view, Teal color scheme_

---

### Cathode
https://github.com/hplant6/cathode-es-de

**Created by**\
Hplant6

**Variants**\
(Recommended) Carousels\
Carousel/Systems, Grid/Games\
System/Carousel, List/Games\
Grids\
Grid/Systems, Carousel/Games\
Grid/Systems, List/Games\
Lists\
List/Systems, Carousel/Games\
List/Systems, Grid/Games

**Color schemes**\
Classic(Red) [High CPU]\
Classic(Red) [Low CPU]\
Classic(Blue) [High CPU]\
Classic(Blue) [Low CPU]\
Classic(Yellow) [High CPU]\
Classic(Yellow) [Low CPU]\
Cyber Neon [High CPU]\
Cyber Neon [Low CPU]\
Cyber Teal [High CPU]\
Cyber Teal [Low CPU]\
Resident Eerie [High CPU]\
Resident Eerie [Low CPU]\
Nautical [High CPU]\
Nautical [Low CPU]\
Grape [High CPU]\
Grape [Low CPU]\
Gameboy [High CPU]\
Gameboy [Low CPU]\
Xbox Live [High CPU]\
Xbox Live[Low CPU]\
Virtualboy [High CPU]\
Virtualboy [Low CPU]\
Orange Phosphor [High CPU]\
Orange Phosphor [Low CPU]

**Font sizes**\
Medium\
Large\
Small\
Extra large

**Aspect ratios**\
16:9\
16:10\
4:3\
21:9

**Transitions**\
Default\
Instant\
Slide\
Fade

![alt text](screenshots/cathode-es-de/cathode-es-de_01.jpg "System view, (Recommended) Carousels variant, Classic(Red) [High CPU] color scheme")
_System view, (Recommended) Carousels variant, Classic(Red) [High CPU] color scheme_

![alt text](screenshots/cathode-es-de/cathode-es-de_02.jpg "Gamelist view, (Recommended) Carousels variant, Classic(Red) [High CPU] color scheme")
_Gamelist view, (Recommended) Carousels variant, Classic(Red) [High CPU] color scheme_

![alt text](screenshots/cathode-es-de/cathode-es-de_03.jpg "System view, List/Systems, Grid/Games variant, Xbox Live [High CPU] color scheme")
_System view, List/Systems, Grid/Games variant, Xbox Live [High CPU] color scheme_

![alt text](screenshots/cathode-es-de/cathode-es-de_04.jpg "Gamelist view, List/Systems, Grid/Games variant, Xbox Live [High CPU] color scheme")
_Gamelist view, List/Systems, Grid/Games variant, Xbox Live [High CPU] color scheme_

---

### Chicuelo (Revisited)
https://github.com/anthonycaccese/chicuelo-revisited-es-de

**Created by**\
ant and Chicuelo

**Variants**\
Artwork: Screenshots\
Artwork: Video\
Artwork: Chicuelo

**Aspect ratios**\
16:9\
16:10

![alt text](screenshots/chicuelo-revisited-es-de/chicuelo-revisited-es-de_01.jpg "System view, Artwork: Chicuelo variant")
_System view, Artwork: Chicuelo variant_

![alt text](screenshots/chicuelo-revisited-es-de/chicuelo-revisited-es-de_02.jpg "System view, Artwork: Screenshots variant")
_System view, Artwork: Screenshots variant_

![alt text](screenshots/chicuelo-revisited-es-de/chicuelo-revisited-es-de_03.jpg "Gamelist view")
_Gamelist view_

---

### CodyWheel
https://github.com/Weestuarty/codywheel-es-de

**Created by**\
Weestuarty

**Variants**\
Text Gamelist\
Carousel Gamelist\
Grid Gamelist

**Color schemes**\
Black n White\
Gold\
Blue\
Gameboy\
Hitman\
Retro\
Stay Classy\
Black n Blue\
Chilled\
Purple Rain\
Rusty\
Sega

**Font sizes**\
Medium\
Extra large

**Aspect ratios**\
16:9\
16:10\
4:3\
21:9

**Transitions**\
Instant w/Fade\
Instant

![alt text](screenshots/codywheel-es-de/codywheel-es-de_01.jpg "System view, Black n White color scheme")
_System view, Black n White color scheme_

![alt text](screenshots/codywheel-es-de/codywheel-es-de_02.jpg "Gamelist view, Text Gamelist variant, Black n White color scheme")
_Gamelist view, Text Gamelist variant, Black n White color scheme_

![alt text](screenshots/codywheel-es-de/codywheel-es-de_03.jpg "System view, Blue color scheme")
_System view, Blue color scheme_

![alt text](screenshots/codywheel-es-de/codywheel-es-de_04.jpg "Gamelist view, Carousel Gamelist variant, Blue color scheme")
_Gamelist view, Carousel Gamelist variant, Blue color scheme_

![alt text](screenshots/codywheel-es-de/codywheel-es-de_05.jpg "System view, Purple Rain color scheme")
_System view, Purple Rain color scheme_

![alt text](screenshots/codywheel-es-de/codywheel-es-de_06.jpg "Gamelist view, Grid Gamelist variant, Purple Rain color scheme")
_Gamelist view, Grid Gamelist variant, Purple Rain color scheme_

---

### CoinOPS
https://github.com/TheGrizzMD/coinops-es-de

**Created by**\
TheGrizzMD, gjsmsmith, BritneysPAIRS and fagnerpc

**Variants**\
Default\
No Hue\
Simple\
Simple With Cover Art\
Default: Grid

**Aspect ratios**\
16:9\
16:10\
4:3\
19.5:9

![alt text](screenshots/coinops-es-de/coinops-es-de_01.jpg "System view, Default variant")
_System view, Default variant_

![alt text](screenshots/coinops-es-de/coinops-es-de_02.jpg "Gamelist view, Default variant")
_Gamelist view, Default variant_

![alt text](screenshots/coinops-es-de/coinops-es-de_03.jpg "System view, Simple variant")
_System view, Simple variant_

![alt text](screenshots/coinops-es-de/coinops-es-de_04.jpg "Gamelist view, Simple variant")
_Gamelist view, Simple variant_

---

### Colorful (Pop!)
https://github.com/RobZombie9043/colorful-pop-es-de

**Created by**\
Rob Zombie, ant and viking

**Variants**\
List (video)\
List (detailed)\
Carousel\
Carousel (video)

**Color schemes**\
[Pop Images] (Colorful) Light\
[Pop Images] (Colorful) Dark\
[Pop Images] (Colorful) Black\
[Pop Images] (Mono) Light\
[Pop Images] (Mono) Dark\
[Pop Images] (Mono) Black\
[Colorful Images] (Colorful) Light\
[Colorful Images] (Colorful) Dark\
[Colorful Images] (Colorful) Black\
[Colorful Images] (Mono) Light\
[Colorful Images] (Mono) Dark\
[Colorful Images] (Mono) Black

**Font sizes**\
Medium\
Large\
Small\
Extra large

**Aspect ratios**\
16:9\
16:10\
3:2\
4:3\
5:4\
19.5:9\
21:9

![alt text](screenshots/colorful-pop-es-de/colorful-pop-es-de_01.jpg "System view, List (detailed) variant, [Pop Images] (Colorful) Dark color scheme")
_System view, List (detailed) variant, [Pop Images] (Colorful) Dark color scheme_

![alt text](screenshots/colorful-pop-es-de/colorful-pop-es-de_02.jpg "Gamelist view, List (detailed) variant, [Pop Images] (Colorful) Dark color scheme")
_Gamelist view, List (detailed) variant, [Pop Images] (Colorful) Dark color scheme_

![alt text](screenshots/colorful-pop-es-de/colorful-pop-es-de_03.jpg "System view, List (video) variant, [Pop Images] (Colorful) Light color scheme")
_System view, List (video) variant, [Pop Images] (Colorful) Light color scheme_

![alt text](screenshots/colorful-pop-es-de/colorful-pop-es-de_04.jpg "Gamelist view, List (video) variant, [Pop Images] (Colorful) Light color scheme")
_Gamelist view, List (video) variant, [Pop Images] (Colorful) Light color scheme_

---

### Colorful (Revisited)
https://github.com/anthonycaccese/colorful-revisited-es-de

**Created by**\
ant and viking

**Color schemes**\
Light\
Dark\
Black

**Aspect ratios**\
16:9\
16:10

![alt text](screenshots/colorful-revisited-es-de/colorful-revisited-es-de_01.jpg "System view, Light color scheme")
_System view, Light color scheme_

![alt text](screenshots/colorful-revisited-es-de/colorful-revisited-es-de_02.jpg "Gamelist view, Light color scheme")
_Gamelist view, Light color scheme_

![alt text](screenshots/colorful-revisited-es-de/colorful-revisited-es-de_03.jpg "System view, Dark color scheme")
_System view, Dark color scheme_

![alt text](screenshots/colorful-revisited-es-de/colorful-revisited-es-de_04.jpg "Gamelist view, Dark color scheme")
_Gamelist view, Dark color scheme_

---

### Colorful (Simplified)
https://github.com/anthonycaccese/colorful-simplified-es-de

**Created by**\
ant and viking

**Variants**\
List: Boxart\
List: Miximage\
List: Screenshot\
List: Screenshot + Marquee\
List: Title Screen\
List: Physical Media\
List: Metadata & Boxart\
List: Metadata & Miximage\
List: Metadata & Screenshot\
List: Metadata & Screenshot + Marquee\
List: Metadata & Title Screen\
List: Metadata & Physical Media\
Carousel: Boxart

**Color schemes**\
Colorful\
Light\
Retro Bright\
DMG\
Dark\
OLED

**Font sizes**\
Medium\
Large

**Aspect ratios**\
16:9\
16:10\
4:3

![alt text](screenshots/colorful-simplified-es-de/colorful-simplified-es-de_01.jpg "System view, List: Boxart variant, Colorful color scheme")
_System view, List: Boxart variant, Colorful color scheme_

![alt text](screenshots/colorful-simplified-es-de/colorful-simplified-es-de_02.jpg "Gamelist view, List: Miximage variant, Colorful color scheme")
_Gamelist view, List: Miximage variant, Colorful color scheme_

![alt text](screenshots/colorful-simplified-es-de/colorful-simplified-es-de_03.jpg "Gamelist view, Carousel: Boxart variant, Dark color scheme")
_Gamelist view, Carousel: Boxart variant, Dark color scheme_

---

### DEcaffe
https://github.com/Weestuarty/decaffe-es-de

**Created by**\
Weestuarty

**Variants**\
Solid w Meta\
Solid w/o Meta\
Color w Meta\
Color w/o Meta

**Color schemes**\
Blue dark\
Yellow\
Classy\
Hyrule\
Blue light\
Purple\
Greyscale

**Font sizes**\
Medium\
Large\
Extra large

**Aspect ratios**\
16:9\
16:10\
4:3\
19.5:9\
21:9

**Transitions**\
Instant w/Fade\
Instant

![alt text](screenshots/decaffe-es-de/decaffe-es-de_01.jpg "System view, Solid w Meta variant, Blue light color scheme")
_System view, Solid w Meta variant, Blue light color scheme_

![alt text](screenshots/decaffe-es-de/decaffe-es-de_02.jpg "Gamelist view, Solid w Meta variant, Blue light color scheme")
_Gamelist view, Solid w Meta variant, Blue light color scheme_

---

### Diamond
https://github.com/Weestuarty/diamond-es-de

**Created by**\
Weestuarty and GreatFlash

**Variants**\
Color w/ text alt\
Color w/ text gamelist\
Color w/ grid gamelist\
Solid w/ text alt\
Solid w/ text gamelist\
Solid w/ grid gameslist

**Color schemes**\
Blue dark\
Yellow\
Classy\
Hyrule\
Blue light\
Purple\
Greyscale

**Font sizes**\
Medium\
Large\
Extra large

**Aspect ratios**\
16:9\
16:10\
4:3\
21:9

**Transitions**\
Slide\
Instant

![alt text](screenshots/diamond-es-de/diamond-es-de_01.jpg "System view, Color w/ text alt variant, Classy color scheme")
_System view, Color w/ text alt variant, Classy color scheme_

![alt text](screenshots/diamond-es-de/diamond-es-de_02.jpg "Gamelist view, Color w/ text alt variant, Classy color scheme")
_Gamelist view, Color w/ text alt variant, Classy color scheme_

![alt text](screenshots/diamond-es-de/diamond-es-de_03.jpg "System view, Color w/ text gamelist variant, Blue dark color scheme")
_System view, Color w/ text gamelist variant, Blue dark color scheme_

![alt text](screenshots/diamond-es-de/diamond-es-de_04.jpg "Gamelist view, Color w/ text gamelist variant, Blue dark color scheme")
_Gamelist view, Color w/ text gamelist variant, Blue dark color scheme_

---

### DS
https://github.com/Weestuarty/ds-es-de

**Created by**\
Weestuarty

**Variants**\
Grid\
Carousel\
R4

**Color schemes**\
DS\
DS Dark\
NES\
Famicom\
Gold\
OLED\
Blueprint

**Font sizes**\
Medium\
Large

**Aspect ratios**\
16:9\
16:10\
4:3\
19.5:9\
1:1

**Transitions**\
Instant\
Slide\
Fade

![alt text](screenshots/ds-es-de/ds-es-de_01.jpg "System view, Grid variant, DS color scheme")
_System view, Grid variant, DS color scheme_

![alt text](screenshots/ds-es-de/ds-es-de_02.jpg "Gamelist view, Grid variant, DS color scheme")
_Gamelist view, Grid variant, DS color scheme_

![alt text](screenshots/ds-es-de/ds-es-de_03.jpg "System view, Carousel variant, Blueprint color scheme")
_System view, Carousel variant, Blueprint color scheme_

![alt text](screenshots/ds-es-de/ds-es-de_04.jpg "Gamelist view, Carousel variant, Blueprint color scheme")
_Gamelist view, Carousel variant, Blueprint color scheme_

---

### Elegance
https://github.com/Weestuarty/elegance-es-de

**Created by**\
Weestuarty

**Variants**\
Solid with Videos\
Solid no Videos\
Color with Videos\
Color no Videos

**Color schemes**\
Yellow\
Classy\
Hyrule\
White/Blue\
Grey/Blue\
Grey/Purple\
White/Red

**Aspect ratios**\
16:9\
16:10\
4:3\
21:9

**Transitions**\
Instant w/Fade\
Instant

![alt text](screenshots/elegance-es-de/elegance-es-de_01.jpg "System view, Yellow color scheme")
_System view, Yellow color scheme_

![alt text](screenshots/elegance-es-de/elegance-es-de_02.jpg "Gamelist view, Solid with Videos variant, Yellow color scheme")
_Gamelist view, Solid with Videos variant, Yellow color scheme_

---

### Elementerial
https://github.com/RobZombie9043/elementerial-es-de

**Created by**\
Rob Zombie and mluizvitor

**Variants**\
[Modern] List: Basic View\
[Modern] List: Video View\
[Modern] List: Detailed View\
[Modern] Elementflix View\
[Modern] Grid View (Small)\
[Modern] Grid View (Large)\
[Classic] List: Basic View\
[Classic] List: Video View\
[Classic] List: Detailed View\
[Classic] Elementflix View\
[Classic] Grid View (Small)\
[Classic] Grid View (Large)\
[Video] List: Basic View\
[Video] List: Video View\
[Video] List: Detailed View\
[Video] Elementflix View\
[Video] Grid View (Small)\
[Video] Grid View (Large)

**Color schemes**\
Dark - Strawberry\
Dark - Orange\
Dark - Banana\
Dark - Lime\
Dark - Mint\
Dark - Blueberry\
Dark - Grape\
Dark - Bubblegum\
Dark - Cocoa\
Dark - Slate\
Dark - SNES\
Dark - Gameboy\
Dark - Pikachu Edition\
Dark - Red Fruits\
Light - Strawberry\
Light - Orange\
Light - Banana\
Light - Lime\
Light - Mint\
Light - Blueberry\
Light - Grape\
Light - Bubblegum\
Light - Cocoa\
Light - Slate\
Light - SNES\
Light - Gameboy\
Light - Pikachu Edition\
Light - Red Fruits

**Font sizes**\
Medium\
Large\
Small

**Aspect ratios**\
16:9\
16:10\
3:2\
4:3\
5:4\
19.5:9\
21:9\
1:1

![alt text](screenshots/elementerial-es-de/elementerial-es-de_01.jpg "System view, [Modern] Elementflix View variant, Dark - Lime color scheme")
_System view, [Modern] Elementflix View variant, Dark - Lime color scheme_

![alt text](screenshots/elementerial-es-de/elementerial-es-de_02.jpg "Gamelist view, [Modern] Elementflix View variant, Dark - Lime color scheme")
_Gamelist view, [Modern] Elementflix View variant, Dark - Lime color scheme_

![alt text](screenshots/elementerial-es-de/elementerial-es-de_03.jpg "System view, [Classic] List: Video View variant, Light - Banana color scheme")
_System view, [Classic] List: Video View variant, Light - Banana color scheme_

![alt text](screenshots/elementerial-es-de/elementerial-es-de_04.jpg "Gamelist view, [Classic] List: Video View variant, Light - Banana color scheme")
_Gamelist view, [Classic] List: Video View variant, Light - Banana color scheme_

---

### Epic Noir (Next)
https://github.com/anthonycaccese/epic-noir-next-es-de

**Created by**\
ant and Chicuelo

**Variants**\
Grid: Marquees\
Grid: Screenshots\
Grid: Physical Media\
Grid: Covers\
Grid: Title Screens\
Grid: No Metadata

**Aspect ratios**\
16:9\
16:10

![alt text](screenshots/epic-noir-next-es-de/epic-noir-next-es-de_01.jpg "System view")
_System view_

![alt text](screenshots/epic-noir-next-es-de/epic-noir-next-es-de_02.jpg "Gamelist view, Grid: Marquees variant")
_Gamelist view, Grid: Marquees variant_

![alt text](screenshots/epic-noir-next-es-de/epic-noir-next-es-de_03.jpg "Gamelist view, Grid: Physical Media variant")
_Gamelist view, Grid: Physical Media variant_

![alt text](screenshots/epic-noir-next-es-de/epic-noir-next-es-de_04.jpg "Gamelist view, Grid: Title Screens variant")
_Gamelist view, Grid: Title Screens variant_

---

### Epic Noir (Revisited)
https://github.com/anthonycaccese/epic-noir-revisited-es-de

**Created by**\
ant, dragoonDorise, c64-dev and Chicuelo

**Aspect ratios**\
16:9\
16:10

![alt text](screenshots/epic-noir-revisited-es-de/epic-noir-revisited-es-de_01.jpg "System view")
_System view_

![alt text](screenshots/epic-noir-revisited-es-de/epic-noir-revisited-es-de_02.jpg "Gamelist view")
_Gamelist view_

---

### ES-Boy
https://github.com/Weestuarty/esboy-es-de

**Created by**\
Weestuarty

**Variants**\
Textlist with videos\
Textlist without videos

**Color schemes**\
Green\
Grey\
Gold

**Font sizes**\
Medium\
Large\
Extra large

**Aspect ratios**\
16:9\
16:10\
4:3\
19.5:9\
21:9

**Transitions**\
Instant\
Slide\
Fade

![alt text](screenshots/esboy-es-de/esboy-es-de_01.jpg "System view, Textlist with videos variant, Gold color scheme")
_System view, Textlist with videos variant, Gold color scheme_

![alt text](screenshots/esboy-es-de/esboy-es-de_02.jpg "Gamelist view, Textlist with videos variant, Gold color scheme")
_Gamelist view, Textlist with videos variant, Gold color scheme_

![alt text](screenshots/esboy-es-de/esboy-es-de_03.jpg "System view, Textlist with videos variant, Green color scheme")
_System view, Textlist with videos variant, Green color scheme_

![alt text](screenshots/esboy-es-de/esboy-es-de_04.jpg "Gamelist view, Textlist with videos variant, Green color scheme")
_Gamelist view, Textlist with videos variant, Green color scheme_

---

### ES-DE-Mini
https://github.com/Weestuarty/mini-es-de

**Created by**\
Weestuarty

**Variants**\
Super Nintendo (List)\
UsnesA UsnesA (List)\
Nintendo (List)\
Mega Drive (List)\
Master System (List)\
PlayStation (List)\
Super Nintendo (Grid)\
UsnesA UsnesA (Grid)\
Nintendo (Grid)\
Mega Drive (Grid)\
Master System (Grid)\
PlayStation (Grid)

**Color schemes**\
SNES\
USNESA\
NES\
Famicon\
Sega\
Master System\
Hystoria\
Hystoria ALT\
Limited Edition\
Its a Mario\
Mrs LEphant\
Gold

**Font sizes**\
Medium\
Large\
Extra large

**Aspect ratios**\
16:9\
16:10\
4:3\
21:9

**Transitions**\
Instant w/Fade\
Instant

![alt text](screenshots/mini-es-de/mini-es-de_01.jpg "System view, Nintendo (List) variant, NES color scheme")
_System view, Nintendo (List) variant, NES color scheme"_

![alt text](screenshots/mini-es-de/mini-es-de_02.jpg "Gamelist view, Nintendo (List) variant, NES color scheme")
_Gamelist view, Nintendo (List) variant, NES color scheme_

![alt text](screenshots/mini-es-de/mini-es-de_03.jpg "System view, PlayStation (List) variant, USNESA color scheme")
_System view, PlayStation (List) variant, USNESA color scheme_

![alt text](screenshots/mini-es-de/mini-es-de_04.jpg "Gamelist view, PlayStation (List) variant, USNESA color scheme")
_Gamelist view, PlayStation (List) variant, USNESA color scheme_

![alt text](screenshots/mini-es-de/mini-es-de_05.jpg "System view, Super Nintendo (Grid) variant, SNES color scheme")
_System view, Super Nintendo (Grid) variant, SNES color scheme_

![alt text](screenshots/mini-es-de/mini-es-de_06.jpg "Gamelist view, Super Nintendo (Grid) variant, SNES color scheme")
_Gamelist view, Super Nintendo (Grid) variant, SNES color scheme_

---

### ES-DWEE
https://github.com/Weestuarty/es-dwee-es-de

**Created by**\
Weestuarty

**Variants**\
Textlist\
Fullscreen

**Color schemes**\
Blue\
Gold\
Rose\
Famiwii\
Retro\
RetroDECK

**Font sizes**\
Medium\
Large

**Aspect ratios**\
16:9\
16:10\
4:3\
19.5:9\
21:9

**Transitions**\
Slide\
Instant

![alt text](screenshots/es-dwee-es-de/es-dwee-es-de_01.jpg "System view, Blue color scheme")
_System view, Blue color scheme_

![alt text](screenshots/es-dwee-es-de/es-dwee-es-de_02.jpg "Gamelist view, Textlist variant, Blue color scheme")
_Gamelist view, Textlist variant, Blue color scheme_

---

### Escape
https://github.com/RobZombie9043/escape-es-de

**Created by**\
Rob Zombie

**Variants**\
Watchtower Distant\
Lakeside\
Watchtower Close-Up

**Color schemes**\
Dawn\
Sunrise\
Early Morning\
Day\
Golden Hour\
Sunset\
Dusk\
Night

**Font sizes**\
Medium\
Large\
Small

**Aspect ratios**\
16:9\
16:10\
3:2\
4:3\
5:4\
19.5:9\
21:9\
32:9\
1:1

**Languages**\
English (United States)\
English (United Kingdom)\
Español (España)\
Français\
Italiano\
Polski\
Português (Brasil)\
Română\
Русский\
Svenska\
日本語\
简体中文

![alt text](screenshots/escape-es-de/escape-es-de_01.jpg "System view, Watchtower Distant variant, Sunset color scheme")
_System view, Watchtower Distant variant, Sunset color scheme_

![alt text](screenshots/escape-es-de/escape-es-de_02.jpg "Gamelist view, Watchtower Distant variant, Night color scheme")
_Gamelist view, Watchtower Distant variant, Night color scheme_

![alt text](screenshots/escape-es-de/escape-es-de_03.jpg "System view, Lakeside variant, Sunset color scheme")
_System view, Lakeside variant, Sunset color scheme_

![alt text](screenshots/escape-es-de/escape-es-de_04.jpg "Gamelist view, Lakeside variant, Night color scheme")
_Gamelist view, Lakeside variant, Night color scheme_

---

### ForAll
https://github.com/Weestuarty/forall-es-de

**Created by**\
Weestuarty

**Variants**\
Dyslexia w videos\
Dyslexia w/o videos\
Impaired w videos\
Impaired w/o videos

**Color schemes**\
Mono\
Mono Variant\
IBM\
IBM Variant\
Bang Wong\
Bang Wong Variant\
Paul Tol\
Paul Tol Variant

**Aspect ratios**\
16:9\
16:10\
4:3\
21:9

**Transitions**\
Instant w/Fade\
Instant

![alt text](screenshots/forall-es-de/forall-es-de_01.jpg "System view, Impaired w videos variant, Bang Wong color scheme")
_System view, Impaired w videos variant, Bang Wong color scheme_

![alt text](screenshots/forall-es-de/forall-es-de_02.jpg "Gamelist view, Impaired w videos variant, Bang Wong color scheme")
_Gamelist view, Impaired w videos variant, Bang Wong color scheme_

![alt text](screenshots/forall-es-de/forall-es-de_03.jpg "System view, Dyslexia w videos variant, Paul Tol Variant color scheme")
_System view, Dyslexia w videos variant, Paul Tol Variant color scheme_

![alt text](screenshots/forall-es-de/forall-es-de_04.jpg "Gamelist view, Dyslexia w videos variant, Paul Tol Variant color scheme")
_Gamelist view, Dyslexia w videos variant, Paul Tol Variant color scheme_

---

### Game OS
https://github.com/lilbud/gameos-es-de

**Created by**\
lilbud and PlayingKarrde

**Variants**\
Default\
Grid (3 Columns)\
Grid (4 Columns)\
Grid (5 Columns)\
Grid (6 Columns)\
Grid (7 Columns)\
Grid (8 Columns)

**Color schemes**\
Game OS\
Dark OS Blue\
Dark OS Brown\
Dark OS Gray\
Dark OS Green\
Dark OS Pink\
Dark OS Red\
Dark OS Light Blue\
Dark OS Light Brown\
Dark OS Light Gray\
Dark OS Light Green\
Dark OS Light Pink\
Dark OS Light Red\
Dark OS Magenta\
Dark OS Orange\
Dark OS Purple\
Dark OS Steel\
Dark OS Stone\
Dark OS Turquoise\
Dark OS Yellow

**Font sizes**\
Medium\
Large

**Aspect ratios**\
16:9\
4:3

![alt text](screenshots/gameos-es-de/gameos-es-de_01.jpg "System view, Game OS color scheme")
_System view, Game OS color scheme_

![alt text](screenshots/gameos-es-de/gameos-es-de_02.jpg "Gamelist view, Default variant, Game OS color scheme")
_Gamelist view, Default variant, Game OS color scheme_

![alt text](screenshots/gameos-es-de/gameos-es-de_03.jpg "System view, Dark OS Light Brown color scheme")
_System view, Dark OS Light Brown color scheme_

![alt text](screenshots/gameos-es-de/gameos-es-de_04.jpg "Gamelist view, Grid (4 Columns) variant, Dark OS Light Brown color scheme")
_Gamelist view, Grid (4 Columns) variant, Dark OS Light Brown color scheme_

---

### Horizon
https://github.com/RobZombie9043/horizon-es-de

**Created by**\
Rob Zombie

**Variants**\
List: Detailed\
List: Basic\
Grid (Small): Detailed\
Grid (Small): Basic\
Grid (Medium): Detailed\
Grid (Medium): Basic\
Grid (Large): Detailed\
Grid (Large): Basic\
Carousel

**Color schemes**\
Default\
Custom

**Font sizes**\
Medium\
Large\
Extra large

**Aspect ratios**\
16:9\
16:10\
3:2\
4:3\
5:4\
19.5:9\
21:9\
32:9\
1:1

![alt text](screenshots/horizon-es-de/horizon-es-de_01.jpg "System view, Carousel variant")
_System view, Carousel variant_

![alt text](screenshots/horizon-es-de/horizon-es-de_02.jpg "Gamelist view, Carousel variant")
_Gamelist view, Carousel variant_

![alt text](screenshots/horizon-es-de/horizon-es-de_03.jpg "System view, List: Basic variant")
_System view, List: Basic variant_

![alt text](screenshots/horizon-es-de/horizon-es-de_04.jpg "Gamelist view, List: Basic variant")
_Gamelist view, List: Basic variant_

---

### Iconic
https://github.com/Siddy212/iconic-es-de

**Created by**\
Siddy212 and Matt S

**Variants**\
Textlist\
Textlist - Flipped\
Carousel\
Grid: Boxart\
Grid: Physical Media\
Grid: Titlescreen\
Grid: Marquee\
[Simple] Textlist\
[Simple] Carousel\
[Simple] Grid: Boxart\
[Simple] Grid: Physical Media

**Color schemes**\
Default - Light\
Default - Dark\
Default - Retro Gray\
Classic - Light\
Classic - Dark\
Classic - Retro Gray\
Modern - Light\
Modern - Dark\
Modern - Retro Gray\
Last Played - Light\
Last Played - Dark\
Last Played - Retro Gray\
Custom - Light\
Custom - Dark\
Custom - Retro Gray

**Font sizes**\
Medium\
Large\
Small\
Extra large

**Aspect ratios**\
16:9\
16:10\
3:2\
4:3\
19.5:9\
21:9\
1:1

![alt text](screenshots/iconic-es-de/iconic-es-de_01.jpg "System view, Modern - Light color scheme")
_System view, Modern - Light color scheme_

![alt text](screenshots/iconic-es-de/iconic-es-de_02.jpg "Gamelist view, Textlist variant, Modern - Light color scheme")
_Gamelist view, Textlist variant, Modern - Light color scheme_

![alt text](screenshots/iconic-es-de/iconic-es-de_03.jpg "Gamelist view, Grid: Boxart variant, Classic - Dark color scheme")
_Gamelist view, Grid: Boxart variant, Classic - Dark color scheme_

![alt text](screenshots/iconic-es-de/iconic-es-de_04.jpg "Gamelist view, Grid: Titlescreen variant, Classic - Dark color scheme")
_Gamelist view, Grid: Titlescreen variant, Classic - Dark color scheme_

---

### Immersive (Revisited)
https://github.com/anthonycaccese/immersive-revisited-es-de

**Created by**\
ant

**Variants**\
Background Screenshot\
Background Video

**Aspect ratios**\
16:9\
16:10\
4:3

![alt text](screenshots/immersive-revisited-es-de/immersive-revisited-es-de_01.jpg "System view")
_System view_

![alt text](screenshots/immersive-revisited-es-de/immersive-revisited-es-de_02.jpg "Gamelist view, Background Screenshot variant")
_Gamelist view, Background Screenshot variant_

![alt text](screenshots/immersive-revisited-es-de/immersive-revisited-es-de_03.jpg "Gamelist view, Background Video variant")
_Gamelist view, Background Video variant_

---

### Mania Menu
https://github.com/anthonycaccese/mania-menu-es-de

**Created by**\
ant

**Variants**\
Video\
Screenshot\
Fanart\
Custom

**Color schemes**\
Yellow\
Blue\
Purple

**Aspect ratios**\
16:9\
16:10\
4:3

![alt text](screenshots/mania-menu-es-de/mania-menu-es-de_01.jpg "System view, Screenshot variant, Yellow color scheme")
_System view, Screenshot variant, Yellow color scheme_

![alt text](screenshots/mania-menu-es-de/mania-menu-es-de_02.jpg "Gamelist view, Fanart variant, Yellow color scheme")
_Gamelist view, Fanart variant, Yellow color scheme_

![alt text](screenshots/mania-menu-es-de/mania-menu-es-de_03.jpg "Gamelist view, Screenshot variant, Blue color scheme")
_Gamelist view, Screenshot variant, Blue color scheme_

---

### MinUI Menu
https://github.com/anthonycaccese/minui-menu-es-de

**Created by**\
ant and Shaun Inman

**Variants**\
List\
List + Boxart\
List + Screenshot

**Color schemes**\
Dark (Bold Font)\
Light (Bold Font)\
Dark (Normal Font)\
Light (Normal Font)\
Pocket DMG\
Pocket Micro\
Amber Terminal\
Green Terminal\
Cyberpunk\
Fallout\
Game Boy DMG\
Custom

**Font sizes**\
Medium\
Large\
Small\
Extra large\
Extra small

**Aspect ratios**\
16:9\
16:10\
3:2\
4:3\
19.5:9\
1:1

**Transitions**\
Instant\
Slide

![alt text](screenshots/minui-menu-es-de/minui-menu-es-de_01.jpg "System view, List + Boxart variant, Pocket Micro color scheme")
_System view, List + Boxart variant, Pocket Micro color scheme_

![alt text](screenshots/minui-menu-es-de/minui-menu-es-de_02.jpg "Gamelist view, List + Boxart variant, Pocket Micro color scheme")
_Gamelist view, List + Boxart variant, Pocket Micro color scheme_

![alt text](screenshots/minui-menu-es-de/minui-menu-es-de_03.jpg "System view, List variant, Light (Bold Font) color scheme")
_System view, List variant, Light (Bold Font) color scheme_

![alt text](screenshots/minui-menu-es-de/minui-menu-es-de_04.jpg "Gamelist view, List variant, Light (Bold Font) color scheme")
_Gamelist view, List variant, Light (Bold Font) color scheme_

![alt text](screenshots/minui-menu-es-de/minui-menu-es-de_05.jpg "System view, List + Boxart variant, Cyberpunk color scheme")
_System view, List + Boxart variant, Cyberpunk color scheme_

![alt text](screenshots/minui-menu-es-de/minui-menu-es-de_06.jpg "Gamelist view, List + Boxart variant, Cyberpunk color scheme")
_Gamelist view, List + Boxart variant, Cyberpunk color scheme_

---

### MiSTer Menu
https://github.com/anthonycaccese/mister-menu-es-de

**Created by**\
ant

**Variants**\
Screenshots Off\
Screenshots On

**Color schemes**\
Crisp Mountain Air\
Test Pattern\
Pocky & Rocky\
Custom

**Aspect ratios**\
16:9\
16:10\
4:3

![alt text](screenshots/mister-menu-es-de/mister-menu-es-de_01.jpg "System view, Crisp Mountain Air color scheme")
_System view, Crisp Mountain Air color scheme_

![alt text](screenshots/mister-menu-es-de/mister-menu-es-de_02.jpg "Gamelist view, Screenshots Off variant, Test Pattern color scheme")
_Gamelist view, Screenshots Off variant, Test Pattern color scheme_

![alt text](screenshots/mister-menu-es-de/mister-menu-es-de_03.jpg "Gamelist view, Screenshots On variant")
_Gamelist view, Screenshots On variant_

---

### Modern
https://gitlab.com/es-de/themes/modern-es-de \
_This theme is bundled with ES-DE on all platforms except Android_

**Created by**\
Sophia Hadash and lilbud

**Variants**\
Textlist with videos\
Textlist without videos\
Textlist with videos (legacy)\
Textlist without videos (legacy)

**Color schemes**\
Dark\
Light

**Font sizes**\
Medium\
Large

**Aspect ratios**\
16:9\
16:10\
4:3\
21:9

**Languages**\
English (United States)\
English (United Kingdom)\
Català\
Deutsch\
Español (España)\
Français\
Italiano\
Polski\
Português (Brasil)\
Română\
Русский\
Svenska\
日本語\
한국어\
简体中文

**Transitions**\
Instant\
Instant and slide\
Instant and fade

![alt text](screenshots/modern-es-de/modern-es-de_01.jpg "System view, Textlist with videos variant, Dark color scheme")
_System view, Textlist with videos variant, Dark color scheme_

![alt text](screenshots/modern-es-de/modern-es-de_02.jpg "Gamelist view, Textlist with videos variant, Dark color scheme")
_Gamelist view, Textlist with videos variant, Dark color scheme_

![alt text](screenshots/modern-es-de/modern-es-de_03.jpg "System view, Textlist with videos variant, Light color scheme")
_System view, Textlist with videos variant, Light color scheme_

![alt text](screenshots/modern-es-de/modern-es-de_04.jpg "Gamelist view, Textlist with videos variant, Light color scheme")
_Gamelist view, Textlist with videos variant, Light color scheme_

![alt text](screenshots/modern-es-de/modern-es-de_05.jpg "System view, Textlist with videos (legacy) variant, Dark color scheme")
_System view, Textlist with videos (legacy) variant, Dark color scheme_

![alt text](screenshots/modern-es-de/modern-es-de_06.jpg "System view, Textlist with videos (legacy) variant, Light color scheme")
_System view, Textlist with videos (legacy) variant, Light color scheme_

---

### Notepad Pocket
https://github.com/anthonycaccese/notepad-pocket-es-de

**Created by**\
Zoidburg and ant

**Variants**\
Polaroid: Screenshot\
Polaroid: Fanart\
Polaroid: Title Screen

**Color schemes**\
Walnut\
Carpet\
Purple\
Cutting Mat

**Font sizes**\
Medium\
Large\
Small

**Aspect ratios**\
16:9\
16:10\
4:3\
19.5:9\
1:1

**Transitions**\
Instant\
Slide

![alt text](screenshots/notepad-pocket-es-de/notepad-pocket-es-de_01.jpg "System view, Polaroid: Screenshot variant, Walnut color scheme")
_System view, Polaroid: Screenshot variant, Walnut color scheme_

![alt text](screenshots/notepad-pocket-es-de/notepad-pocket-es-de_02.jpg "Gamelist view, Polaroid: Screenshot variant, Walnut color scheme")
_Gamelist view, Polaroid: Screenshot variant, Walnut color scheme_

![alt text](screenshots/notepad-pocket-es-de/notepad-pocket-es-de_03.jpg "System view, Polaroid: Title Screen variant, Carpet color scheme")
_System view, Polaroid: Title Screen variant, Carpet color scheme_

![alt text](screenshots/notepad-pocket-es-de/notepad-pocket-es-de_04.jpg "Gamelist view, Polaroid: Title Screen variant, Carpet color scheme")
_Gamelist view, Polaroid: Title Screen variant, Carpet color scheme_

---

### NSO Menu (Interpreted)
https://github.com/anthonycaccese/nso-menu-interpreted-es-de

**Created by**\
ant and rog

**Variants**\
Controller Images\
Colorful Images\
Controller Images + Screenshot\
Colorful Images + Screenshot\
Custom Images

**Color schemes**\
ES-DE Avatar\
Custom Avatar

**Font sizes**\
Medium\
Large

**Aspect ratios**\
16:9\
16:10\
4:3

![alt text](screenshots/nso-menu-interpreted-es-de/nso-menu-interpreted-es-de_01.jpg "System view, Controller Images variant, ES-DE Avatar color scheme")
_System view, Controller Images variant, ES-DE Avatar color scheme_

![alt text](screenshots/nso-menu-interpreted-es-de/nso-menu-interpreted-es-de_02.jpg "System view, Colorful Images + Screenshot variant, ES-DE Avatar color scheme")
_System view, Colorful Images + Screenshot variant, ES-DE Avatar color scheme_

![alt text](screenshots/nso-menu-interpreted-es-de/nso-menu-interpreted-es-de_03.jpg "Gamelist view, ES-DE Avatar color scheme (with rectangular box covers)")
_Gamelist view, ES-DE Avatar color scheme (with rectangular box covers)_

![alt text](screenshots/nso-menu-interpreted-es-de/nso-menu-interpreted-es-de_04.jpg "Gamelist view, ES-DE Avatar color scheme (with square screenshots)")
_Gamelist view, ES-DE Avatar color scheme (with square screenshots)_

---

### P-10 Menu
https://github.com/anthonycaccese/p-10-menu-es-de

**Created by**\
ant

**Variants**\
List\
List + Boxart

**Color schemes**\
Playchoice\
Light Blue\
Capcom Purple\
Retro Gray\
Dark Tan\
OLED\
Light\
Custom

**Aspect ratios**\
16:9\
16:10\
3:2\
4:3\
19.5:9\
1:1

![alt text](screenshots/p-10-menu-es-de/p-10-menu-es-de_01.jpg "System view, Playchoice color scheme")
_System view, Playchoice color scheme_

![alt text](screenshots/p-10-menu-es-de/p-10-menu-es-de_02.jpg "Gamelist view, List variant, Playchoice color scheme")
_Gamelist view, List variant, Playchoice color scheme_

![alt text](screenshots/p-10-menu-es-de/p-10-menu-es-de_03.jpg "System view, Capcom Purple color scheme")
_System view, Capcom Purple color scheme_

![alt text](screenshots/p-10-menu-es-de/p-10-menu-es-de_04.jpg "Gamelist view, List + Boxart variant, Capcom Purple color scheme")
_Gamelist view, List + Boxart variant, Capcom Purple color scheme_

---

### Razor
https://github.com/Weestuarty/razor-es-de

**Created by**\
Weestuarty

**Variants**\
GB w/Metadata(no color)\
Alt w/Metadata(no color)\
LCD w/Metadata(no color)\
GB w/o Metadata (no color)\
Alt w/o Metadata (no color)\
LCD w/o Metadata (no color)\
GB w/Metadata\
LCD w/Metadata\
Alt w/Metadata\
GB w/o Metadata\
LCD w/o Metadata\
Alt w/o Metadata

**Color schemes**\
Game & Watch\
Gameboy\
Classy\
Blue LCD\
Dark\
Yellow

**Font sizes**\
Medium\
Large\
Extra large

**Aspect ratios**\
16:9\
16:10\
4:3\
21:9

**Transitions**\
Instant\
Instant and slide\
Instant and fade

![alt text](screenshots/razor-es-de/razor-es-de_01.jpg "System view, GB w/o Metadata variant, Classy color scheme")
_System view, GB w/o Metadata variant, Classy color scheme_

![alt text](screenshots/razor-es-de/razor-es-de_02.jpg "Gamelist view, GB w/o Metadata variant, Classy color scheme")
_Gamelist view, GB w/o Metadata variant, Classy color scheme_

![alt text](screenshots/razor-es-de/razor-es-de_03.jpg "System view, LCD w/Metadata variant, Gameboy color scheme")
_System view, LCD w/Metadata variant, Gameboy color scheme_

![alt text](screenshots/razor-es-de/razor-es-de_04.jpg "Gamelist view, LCD w/Metadata variant, Gameboy color scheme")
_Gamelist view, LCD w/Metadata variant, Gameboy color scheme_

![alt text](screenshots/razor-es-de/razor-es-de_05.jpg "System view, GB w/Metadata variant, Dark color scheme")
_System view, GB w/Metadata variant, Dark color scheme_

![alt text](screenshots/razor-es-de/razor-es-de_06.jpg "Gamelist view, GB w/Metadata variant, Dark color scheme")
_Gamelist view, GB w/Metadata variant, Dark color scheme_

---

### Retro Mega (Revisited)
https://github.com/anthonycaccese/retromega-revisited-es-de

**Created by**\
ant and djfumberger

**Variants**\
List: Boxart\
List: Miximage\
List: Screenshot\
List: Title Screen\
List: Physical Media\
Carousel: Boxart\
Basic

**Color schemes**\
Colorful (Light)\
Colorful (Dark)\
DMG\
Famicom\
Indigo (Light)\
Indigo (Dark)\
Retro

**Font sizes**\
Medium\
Small

**Aspect ratios**\
16:9\
16:10\
3:2\
4:3\
19.5:9\
21:9

**Transitions**\
Slide\
Instant + Slide\
Instant

![alt text](screenshots/retromega-revisited-es-de/retromega-revisited-es-de_01.jpg "System view, List: Boxart variant, Colorful (Light) color scheme")
_System view, List: Boxart variant, Colorful (Light) color scheme_

![alt text](screenshots/retromega-revisited-es-de/retromega-revisited-es-de_02.jpg "Gamelist view, List: Boxart variant, Colorful (Light) color scheme")
_Gamelist view, List: Boxart variant, Colorful (Light) color scheme_

![alt text](screenshots/retromega-revisited-es-de/retromega-revisited-es-de_03.jpg "System view, List: Title Screen variant, Famicom color scheme")
_System view, List: Title Screen variant, Famicom color scheme_

![alt text](screenshots/retromega-revisited-es-de/retromega-revisited-es-de_04.jpg "Gamelist view, List: Title Screen variant, Famicom color scheme")
_Gamelist view, List: Title Screen variant, Famicom color scheme_

---

### Retrofix (Revisited)
https://github.com/anthonycaccese/retrofix-revisited-es-de

**Created by**\
ant and 20GotoTen

**Variants**\
Immersive\
Simple\
Carousel\
Grid

**Aspect ratios**\
16:9\
16:10

![alt text](screenshots/retrofix-revisited-es-de/retrofix-revisited-es-de_01.jpg "System view")
_System view_

![alt text](screenshots/retrofix-revisited-es-de/retrofix-revisited-es-de_02.jpg "Gamelist view, Immersive variant")
_Gamelist view, Immersive variant_

![alt text](screenshots/retrofix-revisited-es-de/retrofix-revisited-es-de_03.jpg "Gamelist view, Simple variant")
_Gamelist view, Simple variant_

![alt text](screenshots/retrofix-revisited-es-de/retrofix-revisited-es-de_04.jpg "Gamelist view, Carousel variant")
_Gamelist view, Carousel variant_

![alt text](screenshots/retrofix-revisited-es-de/retrofix-revisited-es-de_05.jpg "Gamelist view, Grid variant")
_Gamelist view, Grid variant_

---

### Saga Diamond
https://github.com/Jetup13/sagadiamond-es-de

**Created by**\
Jetup

**Variants**\
Default\
w/ Videos\
w/ Videos & w/o Shadow\
w/ Videos & w/o Shadow/Metadata\
w/o System Metadata\
w/o Shadow\
w/o System Metadata & Shadow

**Color schemes**\
White\
Mint\
Baby Blue\
Orange\
Cardinal Red\
Fuchsia Pink

**Font sizes**\
Medium\
Large

**Aspect ratios**\
16:9\
16:10\
4:3\
19.5:9\
21:9\
1:1

**Languages**\
English (United States)\
Español (España)\
Français\
Italiano\
Polski\
Português (Brasil)\
Română\
Русский\
Svenska\
日本語\
简体中文

**Transitions**\
Instant\
Slide\
Fade

![alt text](screenshots/sagadiamond-es-de/sagadiamond-es-de_01.jpg "System view, Default variant, White color scheme")
_System view, Default variant, White color scheme_

![alt text](screenshots/sagadiamond-es-de/sagadiamond-es-de_02.jpg "Gamelist view, Default variant, White color scheme")
_Gamelist view, Default variant, White color scheme_

![alt text](screenshots/sagadiamond-es-de/sagadiamond-es-de_03.jpg "System view, w/o System Metadata & Shadow variant, Orange color scheme")
_System view, w/o System Metadata & Shadow variant, Orange color scheme_

![alt text](screenshots/sagadiamond-es-de/sagadiamond-es-de_04.jpg "Gamelist view, w/o System Metadata & Shadow variant, Orange color scheme")
_Gamelist view, w/o System Metadata & Shadow variant, Orange color scheme_

---

### shinretro (Revisited)
https://github.com/RobZombie9043/shinretro-revisited-es-de

**Created by**\
Rob Zombie and ant

**Variants**\
Grid\
Carousel

**Color schemes**\
Dark\
Light\
SteamOS\
shinretro Dark

**Font sizes**\
Medium\
Large

**Aspect ratios**\
16:9\
16:10\
4:3\
19.5:9\
1:1

![alt text](screenshots/shinretro-revisited-es-de/shinretro-revisited-es-de_01.jpg "System view, Grid variant, SteamOS color scheme")
_System view, Grid variant, SteamOS color scheme_

![alt text](screenshots/shinretro-revisited-es-de/shinretro-revisited-es-de_02.jpg "Gamelist view, Grid variant, SteamOS color scheme")
_Gamelist view, Grid variant, SteamOS color scheme_

![alt text](screenshots/shinretro-revisited-es-de/shinretro-revisited-es-de_03.jpg "System view, Carousel variant, shinretro Dark color scheme")
_System view, Carousel variant, shinretro Dark color scheme_

![alt text](screenshots/shinretro-revisited-es-de/shinretro-revisited-es-de_04.jpg "Gamelist view, Carousel variant, shinretro Dark color scheme")
_Gamelist view, Carousel variant, shinretro Dark color scheme_

---

### Showcase
https://github.com/Weestuarty/showcase-es-de

**Created by**\
Weestuarty

**Variants**\
With per system bands\
With static bands

**Color schemes**\
Greyscale\
Red\
Blue\
Olive\
Golden\
Weestuarty

**Aspect ratios**\
16:9\
16:10\
4:3\
21:9

![alt text](screenshots/showcase-es-de/showcase-es-de_01.jpg "System view, With per system bands variant, Greyscale color scheme")
_System view, With per system bands variant, Greyscale color scheme_

![alt text](screenshots/showcase-es-de/showcase-es-de_02.jpg "Gamelist view, With per system bands variant, Greyscale color scheme")
_Gamelist view, With per system bands variant, Greyscale color scheme_

![alt text](screenshots/showcase-es-de/showcase-es-de_03.jpg "System view, With per system bands variant, Blue color scheme")
_System view, With per system bands variant, Blue color scheme_

![alt text](screenshots/showcase-es-de/showcase-es-de_04.jpg "Gamelist view, With per system bands variant, Blue color scheme")
_Gamelist view, With per system bands variant, Blue color scheme_

![alt text](screenshots/showcase-es-de/showcase-es-de_05.jpg "System view, With per system bands variant, Olive color scheme")
_System view, With per system bands variant, Olive color scheme_

![alt text](screenshots/showcase-es-de/showcase-es-de_06.jpg "Gamelist view, With per system bands variant, Olive color scheme")
_Gamelist view, With per system bands variant, Olive color scheme_

---

### SimCar
https://github.com/Weestuarty/simcar-es-de

**Created by**\
Weestuarty

**Variants**\
With videos\
Without videos

**Color schemes**\
Gold\
Black & white\
Blue\
Gameboy\
Gameboy Shell\
Retro\
Stay Classy\
Commodore 64\
Chilled\
Purple Rain\
Rusty\
Sega

**Aspect ratios**\
16:9\
16:10\
4:3\
21:9

**Transitions**\
Instant w/Fade\
Instant

![alt text](screenshots/simcar-es-de/simcar-es-de_01.jpg "System view, Gold color scheme")
_System view, Gold color scheme_

![alt text](screenshots/simcar-es-de/simcar-es-de_02.jpg "Gamelist view, With videos variant, Gold color scheme")
_Gamelist view, With videos variant, Gold color scheme_

![alt text](screenshots/simcar-es-de/simcar-es-de_03.jpg "System view, Commodore 64 color scheme")
_System view, Commodore 64 color scheme_

![alt text](screenshots/simcar-es-de/simcar-es-de_04.jpg "Gamelist view, With videos variant, Commodore 64 color scheme")
_Gamelist view, With videos variant, Commodore 64 color scheme_

![alt text](screenshots/simcar-es-de/simcar-es-de_05.jpg "System view, Sega color scheme")
_System view, Sega color scheme_

![alt text](screenshots/simcar-es-de/simcar-es-de_06.jpg "Gamelist view, Without videos variant, Sega color scheme")
_Gamelist view, Without videos variant, Sega color scheme_

---

### SimpleMenu
https://github.com/Rose22/simplemenu-es-de

**Created by**\
Rose22

**Variants**\
Default background | Images behind game list\
Default background | Videos behind game list\
Default background | No media in game list\
Custom background | Images behind game list\
Custom background | Videos behind game list\
Custom background | No media in game list\
No background & No media

**Color schemes**\
Monochrome\
Red\
Green\
Blue\
Gold\
Pink\
Purple

![alt text](screenshots/simplemenu-es-de/simplemenu-es-de_01.jpg "System view, Default background | Images behind game list variant, Gold color scheme")
_System view, Default background | Images behind game list variant, Gold color scheme_

![alt text](screenshots/simplemenu-es-de/simplemenu-es-de_02.jpg "Gamelist view, Default background | Images behind game list variant, Gold color scheme")
_Gamelist view, Default background | Images behind game list variant, Gold color scheme_

![alt text](screenshots/simplemenu-es-de/simplemenu-es-de_03.jpg "System view, Default background | Videos behind game list variant, Monochrome color scheme")
_System view, Default background | Videos behind game list variant, Monochrome color scheme_

![alt text](screenshots/simplemenu-es-de/simplemenu-es-de_04.jpg "Gamelist view, Default background | Videos behind game list variant, Monochrome color scheme")
_Gamelist view, Default background | Videos behind game list variant, Monochrome color scheme_

---

### Slate
https://gitlab.com/es-de/themes/slate-es-de \
_This theme is bundled with ES-DE on all platforms except Android_

**Created by**\
ES-DE project and Recalbox project

**Variants**\
Textlist with videos\
Textlist without videos

**Color schemes**\
Dark\
Light

**Font sizes**\
Medium\
Large

**Aspect ratios**\
16:9\
16:9 vertical\
4:3\
4:3 vertical

**Languages**\
English (United States)\
English (United Kingdom)\
Català\
Deutsch\
Español (España)\
Français\
Italiano\
Polski\
Português (Brasil)\
Română\
Русский\
Svenska\
日本語\
한국어\
简体中文

**Transitions**\
Slide\
Instant\
Fade

![alt text](screenshots/slate-es-de/slate-es-de_01.jpg "System view, Dark mode color scheme")
_System view, Dark mode color scheme_

![alt text](screenshots/slate-es-de/slate-es-de_02.jpg "Gamelist view, Dark mode color scheme")
_Gamelist view, Dark mode color scheme_

![alt text](screenshots/slate-es-de/slate-es-de_03.jpg "System view, Light mode color scheme")
_System view, Light mode color scheme_

![alt text](screenshots/slate-es-de/slate-es-de_04.jpg "Gamelist view, Light mode color scheme")
_Gamelist view, Light mode color scheme_

---

### Slick (Remixed)
https://github.com/Weestuarty/slick-es-de

**Created by**\
Weestuarty and buzz

**Variants**\
Grid Gamelist\
Carousel Gamelist\
Text Gamelist

**Font sizes**\
Medium\
Large

**Aspect ratios**\
16:9\
16:10\
4:3\
19.5:9\
21:9

**Transitions**\
Instant\
Slide

![alt text](screenshots/slick-es-de/slick-es-de_01.jpg "System view, Grid Gamelist variant")
_System view, Grid Gamelist variant_

![alt text](screenshots/slick-es-de/slick-es-de_02.jpg "Gamelist view, Grid Gamelist variant")
_Gamelist view, Grid Gamelist variant_

![alt text](screenshots/slick-es-de/slick-es-de_03.jpg "Gamelist view, Carousel Gamelist variant")
_Gamelist view, Carousel Gamelist variant_

![alt text](screenshots/slick-es-de/slick-es-de_04.jpg "Gamelist view, Text Gamelist variant")
_Gamelist view, Text Gamelist variant_

---

### TateGriddy
https://github.com/Weestuarty/tategriddy-es-de

**Created by**\
Weestuarty

**Variants**\
Grid with videos\
Grid without videos\
Carousel with videos\
Carousel without videos

**Color schemes**\
Greyscale\
Blue\
Gold\
Retro\
Stay Classy\
Chilled\
Purple Rain\
Primary

**Aspect ratios**\
16:9 vertical\
16:10 vertical\
4:3 vertical

**Transitions**\
Instant w/Fade\
Instant

![alt text](screenshots/tategriddy-es-de/tategriddy-es-de_01.jpg "System view, Grid with videos variant, Chilled color scheme")
_System view, Grid with videos variant, Chilled color scheme_

![alt text](screenshots/tategriddy-es-de/tategriddy-es-de_02.jpg "Gamelist view, Grid with videos variant, Chilled color scheme")
_Gamelist view, Grid with videos variant, Chilled color scheme_

![alt text](screenshots/tategriddy-es-de/tategriddy-es-de_03.jpg "System view, Carousel with videos variant, Retro color scheme")
_System view, Carousel with videos variant, Retro color scheme_

![alt text](screenshots/tategriddy-es-de/tategriddy-es-de_04.jpg "Gamelist view, Carousel with videos variant, Retro color scheme")
_Gamelist view, Carousel with videos variant, Retro color scheme_

---

### TechDweeb
https://github.com/anthonycaccese/techdweeb-es-de

**Created by**\
TechDweeb and ant

**Variants**\
List: Metadata, Screenshot & Marquee\
List: Metadata, Screenshot & Boxart\
List: Screenshot\
Carousel: Boxart

**Color schemes**\
Orange Stuff\
Purple Slime\
Aqua Slush\
Green Ooze\
Pink Fluff\
Blue Goo\
Red Squish\
Grey Gunk\
DMG Dungeon\
Custom Chaos

**Font sizes**\
Medium\
Large\
Small

**Aspect ratios**\
16:9\
16:10\
3:2\
4:3\
1:1

![alt text](screenshots/techdweeb-es-de/techdweeb-es-de_01.png "System view, List: Metadata, Screenshot & Marquee variant, Orange Stuff color scheme")
_System view, List: Metadata, Screenshot & Marquee variant, Orange Stuff color scheme_

![alt text](screenshots/techdweeb-es-de/techdweeb-es-de_02.png "Gamelist view, List: Metadata, Screenshot & Marquee variant, Orange Stuff color scheme")
_Gamelist view, List: Metadata, Screenshot & Marquee variant, Orange Stuff color scheme_

![alt text](screenshots/techdweeb-es-de/techdweeb-es-de_03.png "System view, List: Screenshot variant, Blue Goo color scheme")
_System view, List: Screenshot variant, Blue Goo color scheme_

![alt text](screenshots/techdweeb-es-de/techdweeb-es-de_04.png "Gamelist view, List: Screenshot variant, Blue Goo color scheme")
_Gamelist view, List: Screenshot variant, Blue Goo color scheme_

---

### TexGriddy
https://github.com/Weestuarty/texgriddy-es-de

**Created by**\
Weestuarty

**Variants**\
Grid with videos\
Grid without videos\
Textlist with videos\
Textlist without videos

**Color schemes**\
Greyscale\
Blue\
Gold\
Retro\
Stay Classy\
Chilled\
Purple Rain\
Primary

**Font sizes**\
Medium\
Large\
Extra large

**Aspect ratios**\
16:9\
16:10\
4:3\
21:9

**Transitions**\
Instant w/Fade\
Instant

![alt text](screenshots/texgriddy-es-de/texgriddy-es-de_01.jpg "System view, Grid with videos variant, Chilled color scheme")
_System view, Grid with videos variant, Chilled color scheme_

![alt text](screenshots/texgriddy-es-de/texgriddy-es-de_02.jpg "Gamelist view, Grid with videos variant, Chilled color scheme")
_Gamelist view, Grid with videos variant, Chilled color scheme_

![alt text](screenshots/texgriddy-es-de/texgriddy-es-de_03.jpg "Gamelist view, Grid with videos variant, Greyscale color scheme")
_Gamelist view, Grid with videos variant, Greyscale color scheme_

![alt text](screenshots/texgriddy-es-de/texgriddy-es-de_04.jpg "System view, Textlist with videos variant, Retro color scheme")
_System view, Textlist with videos variant, Retro color scheme_

![alt text](screenshots/texgriddy-es-de/texgriddy-es-de_05.jpg "Gamelist view, Textlist with videos variant, Retro color scheme")
_Gamelist view, Textlist with videos variant, Retro color scheme_

---

### Theme engine examples (for theme developers)
https://gitlab.com/es-de/themes/theme-engine-examples-es-de

**Variants**\
01 - Horizontal carousel and textlist\
02 - System view game info and wheel carousels\
03 - Textlist and vertical carousel\
04 - Textlist and carousel with reflections\
05 - System and gamelist grids\
06 - GIF and Lottie animations

**Color schemes**\
Dark\
Light

**Aspect ratios**\
16:9\
4:3

**Transitions**\
Slide\
Instant\
Fade

**Comment**\
Official theme intended for theme developers to learn how to use various ES-DE theme engine capabilities.

![alt text](screenshots/theme-engine-examples-es-de/theme-engine-examples-es-de_01.jpg "System view, 02 - System view game info and wheel carousels variant")
_System view, 02 - System view game info and wheel carousels variant_

![alt text](screenshots/theme-engine-examples-es-de/theme-engine-examples-es-de_02.jpg "Gamelist view, 02 - System view game info and wheel carousels variant")
_Gamelist view, 02 - System view game info and wheel carousels variant_

![alt text](screenshots/theme-engine-examples-es-de/theme-engine-examples-es-de_03.jpg "System view, 03 - Textlist and vertical carousel variant")
_System view, 03 - Textlist and vertical carousel variant_

![alt text](screenshots/theme-engine-examples-es-de/theme-engine-examples-es-de_04.jpg "Gamelist view, 03 - Textlist and vertical carousel variant")
_Gamelist view, 03 - Textlist and vertical carousel variant_

![alt text](screenshots/theme-engine-examples-es-de/theme-engine-examples-es-de_05.jpg "Gamelist view, 04 - Textlist and carousel with reflections variant")
_Gamelist view, 04 - Textlist and carousel with reflections variant_

![alt text](screenshots/theme-engine-examples-es-de/theme-engine-examples-es-de_06.jpg "Gamelist view, 05 - System and gamelist grids variant")
_Gamelist view, 05 - System and gamelist grids variant_

---

### Additional themes

The selection above does not contain every theme available for ES-DE as it's a curated list. There are more themes that can be found on the Internet, below are links to some of them.

To install a theme hosted on GitHub, go to its repository, press the green _Code_ button in the upper right corner and choose _Download ZIP_. For themes hosted on GitLab the small difference is that the download button is not green and that it has a download symbol instead of the text _Code_. You then simply unpack the file into `ES-DE/themes/` and restart ES-DE. See the themes section of the [User guide](https://gitlab.com/es-de/emulationstation-de/-/blob/master/USERGUIDE.md#themes) for additional details and instructions.

https://github.com/RickAndTired/1 \
https://github.com/lilbud/flat-es-de \
https://github.com/Weestuarty/kofe-es-de \
https://github.com/lilbud/material-es-de \
https://github.com/Weestuarty/museum-es-de \
https://github.com/beelzebud/NeOmegaDrive-ES-DE \
https://github.com/m33ts4k0z/retrorama-turbo-es-de
